
// Get the cube of 2
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

// Display address
let address = ["258 Washington Ave NW", "California 90011"];

let [streetName, stateName] = address;
console.log(`I live at ${streetName}, ${stateName}`);

// Introducing Lolong
let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: "1075 kgs",
	size: "20 ft 3 in"
};

let {name, type, weight, size} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${size}.`);

// Display and total given numbers
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((item) => {
    console.log(`${item}`);
});

let reduceNumber = numbers.reduce((total, item) => {
	return total + item;
}, 0)
console.log(`${reduceNumber}`);

// Display Dog details
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);